-- iot_inventory_tables_2016_11_16.sql

-- cells
DROP TABLE IF EXISTS iot_inventory.cells;

CREATE TABLE IF NOT EXISTS iot_inventory.cells
(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 cell_address VARCHAR(32) NOT NULL,
 part_number VARCHAR(32) NOT NULL,
 part_name VARCHAR(32) NOT NULL,
 unit VARCHAR(32) NOT NULL,
 unit_price FLOAT NOT NULL,
 mfc_part_number VARCHAR(32) NOT NULL,
 full_weight FLOAT NOT NULL,
 min_weight FLOAT NOT NULL,
 alarm_weight FLOAT NOT NULL
 );


-- inventory_monitor
-- DROP TABLE IF EXISTS iot_inventory.inventory;
DROP TABLE IF EXISTS iot_inventory.inventory_monitor;

CREATE TABLE IF NOT EXISTS iot_inventory.inventory_monitor
(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 cell_id INT UNSIGNED NOT NULL,
 quantity FLOAT NOT NULL,
 weight FLOAT NOT NULL,
 CONSTRAINT fk_cell_id FOREIGN KEY (cell_id) REFERENCES cells (id) ON UPDATE CASCADE
 );
 
 -- users
DROP TABLE IF EXISTS iot_inventory.users;
 
CREATE TABLE IF NOT EXISTS iot_inventory.users
(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 emplyee_id VARCHAR(32) NOT NULL,
 first_name VARCHAR(32) NOT NULL,
 last_name VARCHAR(32) NOT NULL,
 email VARCHAR(32) NOT NULL,
 phone VARCHAR(32) NOT NULL,
 access_group ENUM('User', 'Purchaser', 'Admin') DEFAULT 'User' NOT NULL
 );

 -- vendors
 DROP TABLE IF EXISTS iot_inventory.vendors;
 
CREATE TABLE IF NOT EXISTS iot_inventory.vendors
(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 vendor_name VARCHAR(32) NOT NULL,
 phone VARCHAR(32) NOT NULL,
 email VARCHAR(32) NOT NULL
);

-- purchaser
DROP TABLE IF EXISTS iot_inventory.purchaser;

CREATE TABLE IF NOT EXISTS iot_inventory.purchaser
(id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
 created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
 updated_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
 user_id INT UNSIGNED NOT NULL,
 vendor_id INT UNSIGNED NOT NULL,
 CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES users (id) ON UPDATE CASCADE,
 CONSTRAINT fk_vendor_id FOREIGN KEY (vendor_id) REFERENCES vendors (id) ON UPDATE CASCADE
 );

/*

-- cells
INSERT iot_inventory.cells
       (cell_address, part_number)
VALUES ('A01', 'P001');

-- inventory
INSERT iot_inventory.inventory
       (cell_id, quantity, weight)
VALUES (1, 1, 10),
       (1, 1, 9),
       (1, 1, 8);

INSERT iot_inventory.inventory
       (cell_id, quantity, weight)
VALUES (2, 1, 10);

SELECT *
FROM iot_inventory.inventory
ORDER BY 1;
*/