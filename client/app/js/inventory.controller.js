(function(){
    angular
        .module("InventoryApp")
        .controller("InventoryCtrl", InventoryCtrl);

    InventoryCtrl.$inject = ["$state", "AuthFactory", "InventoryService"];
    function InventoryCtrl($state, AuthFactory, InventoryService){
        var vm = this;
        // page status: 1 - home page, 2 - cell page, 3 - setting page
        //              4 - login,     5 - register,  6 - cells manager
        vm.pageStatus = 1;

        vm.homepageClicked = function(){
            vm.pageStatus = 1;
        };
        vm.cellpageClicked = function(){
            vm.pageStatus = 2;
        };
        vm.settingpageClicked = function(){
            vm.pageStatus = 3;
        };
        vm.cellsManagerClicked = function(){
            vm.pageStatus = 6;
        };
        vm.partsManagerClicked = function(){
            vm.pageStatus = 7;
        };


        // home page search options
        // 1 - All
        // 2 - Node
        // 3 - Cell
        // 4 - Part
        // 6 - Manufacture
        // 7 - Vendor
        vm.home_options = 1;

        vm.user = {};

        // AuthFactory.getUserStatus(function(result){
        //     vm.isUserLogon = result;
        // });
        vm.isUserLogon = AuthFactory.isLoggedIn();
        console.log("inventory check log in: " + vm.isUserLogon);
        vm.isAdmin = AuthFactory.isAdmin();
        vm.employee_id = AuthFactory.getEmployeeId();
        vm.email = AuthFactory.getEmail();
        vm.username = vm.email;
//        vm.username.slice(0, vm.email.search("@"));
        vm.init = function(){
            if(vm.isUserLogon){
                console.log("index_init_to_home")
                vm.pageStatus = 1;
                $state.go("home");
            }else{
                console.log("index_init_to_login")
                vm.pageStatus = 4;
                $state.go("login");
            }
        };
        vm.logout = function () {
            AuthFactory.logout()
                .then(function () {
                    $state.go("login");
                }).catch(function () {
                console.error("Error logging on !");
            });
        };

        // for dashboard Moving searching form to nav bar, but not working
        vm.searchKey = "";
        vm.searchGroupList = [
            {id: "1", name: "All"},
            {id: "2", name: "By Node Address"},
            {id: "3", name: "By Cell Address"},
            {id: "4", name: "By Part Name"},
            {id: "5", name: "By Part Number"},
            {id: "6", name: "By Mfc Part Name"}
        ];
        vm.searchGroup = "1";
        vm.submit = function () {
            InventoryService.setDashboardParams(vm.searchGroup, vm.searchKey);
            $state.go("home");

        }
    }
})();