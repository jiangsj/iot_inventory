(function(){
    angular
        .module("InventoryApp")
        .controller("SettingCtrl", SettingCtrl);

    SettingCtrl.$inject = ["$http", "$state", "AuthFactory"];
    function SettingCtrl($http, $state, AuthFactory){
        var vm = this;
        vm.inpputInvalid = false;
        vm.status = {
            message: "",
            code: 0
        };
        vm.settingList = [
            {id: "1", name: "Manage Cells"},
            {id: "2", name: "Manage Parts"},
            {id: "3", name: "Manage Vendors"},
            {id: "4", name: "Manage Users"}
        ];
        vm.settingOption = vm.settingList[0].id;
        vm.username = "User Name";
        vm.logout = {
            model: "",
            options: [vm.username, "Log out"]
        };
        vm.data = [];
        vm.cellData = {
            partNo: "",
            mfcPartNo: "",
            fullWeight: 0.0,
            minWeight: 0.0,
            alarmWeight: 0.0,
            vendorName: "",
            vendorPartNo: "",
            unit: "",
            unitPrice: 0.0,
            unitWeight: 0.0
        };

        vm.isUserLogon = AuthFactory.isLoggedIn();

        vm.update = function(){
            console.log("--update()--");
            // var defer = $q.defer;
            $http
                .put("/api/setting" , vm.cellData)
                .then(function(result){
                    console.log("searched success");
                    vm.data = result.data;
                })
                .catch(function(err){
                    console.log("search failed");
                });
            // return defer.promise;
        };

        vm.checkPartNo = function(){
            console.log("--checkPartNo()--");
            // var defer = $q.defer;
            $http
                .put("/api/setting/" + vm.cellData.partNo)
                .then(function(result){
                    console.log("searched success");
                    vm.data = result.data;
                })
                .catch(function(err){
                    console.log("search failed");
                });
            // return defer.promise;
        };

        vm.add = function() {
            console.log("--add()--");
            // var defer = $q.defer;
            $http
                .get("/api/setting/add", vm.cellData)
                .then(function(result){
                    console.log("add successful")
                })
                .catch(function(err){
                    console.log("add failed")
                })
        };

        vm.deleteInventory = function() {
            console.log("--deleteInventory()--");
            // var defer = $q.defer;
            $state.go("/home");
            // $http
            //     .delete("/api/inventory_static/" + vm.data.id)
            //     .then(function(result){
            //         console.log("delete successful");
            //         $state.go("/home");
            //     })
            //     .catch(function(err){
            //         console.log("delete failed")
            //     })

        };
    }
})();