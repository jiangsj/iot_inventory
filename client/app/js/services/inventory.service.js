(function(){
    angular
        .module("InventoryApp")
        .service("InventoryService", InventoryService);

    InventoryService.$inject = ["$http", "$state", "$q"];
    function InventoryService($http, $state, $q){
        var self = this;
        self.cellChartData = [];
        self.cellData = [];
        self.inventory_monitors = [];
        self.cellcounts = 20;
        self.partsData =[];
        self.cell_id = 0;

        self.setCellId = function(cell_id){
            self.cell_id = Number(cell_id);
        };

        self.getCellChartData = function(cellId, limit, dateStart, dateEnd){
            var defer = $q.defer();
            $http
                .get("/api/inventory_monitors", {params: {cell_id: cellId, limit: limit, dateStart: dateStart, dateEnd: dateEnd}})
                .then(function(result){
                    console.log("searched success");
                    self.cellChartData = result.data;
                    console.log("self.cellChartData", self.cellChartData);
                    defer.resolve(result);
                })
                .catch(function(err){
                    console.log("search failed");
                    defer.reject(err);
                });
            return defer.promise;
        };

        self.getCells = function (option, key) {
            var params = {};
            if(key){
                params = {searchOption: option, key: key};
            }
            var defer = $q.defer();
            $http
                .get("/api/cells", {params: params})
                .then(function(result){
                    console.log("search successful");
                    self.cellData = result.data;
                    if(!key){
                        // record total cell counts
                        self.cellcounts = self.cellData.length;
                    }
                    defer.resolve(result);
                })
                .catch(function(err){
                    console.log("search failed");
                    defer.reject(err);
                });
            return defer.promise;
        };

        // accept single cell object, using .create
        // or cells arrary, using bulkcreate
        self.addCells = function (cells) {
            console.log("--inventory.service--addCells--", cells);
            var deferred = $q.defer();
            $http
                .post("/api/cells", cells)
                .then(function(result){
                    console.log("add cells successful");
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("add cells failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        self.updateCell = function (cell) {
            var deferred = $q.defer();
            $http
                .put("/api/cells/" + cell.id, cell)
                .then(function(result){
                    console.log("update successful");
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("update failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        self.bulkUpdateCells = function (cells) {
            var deferred = $q.defer();
            $http
                .put("/api/cells", cells)
                .then(function(result){
                    console.log("bulk update successful");
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("bulk update failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        // may combine getCellChartData
        self.getInventoryWeight = function (queryParams) {
            var params = queryParams;
            // if(queryParams.cellId !== undefined){
            //     // query a particular cell by specified date range
            //     params = {
            //         params: {
            //             cell_id: cellId,
            //             start_date: startDate,
            //             end_date: endDate
            //         }
            //     }
            // }else{
            //     // query latest weight for all cells
            //     params = {params: {cellcounts: self.cellcounts}};
            // }
            var deferred = $q.defer();
            $http
                .get("/api/inventory_monitors", {params: params})
                .then(function(result){
                    console.log("searched success");
                    self.inventory_monitors = result.data;
                    console.log("self.inventory_monitors", self.inventory_monitors);
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("search failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        self.addWeights = function (weidghts) {
            var deferred = $q.defer();
            $http
                .post("/api/inventory_monitors", weidghts)
                .then(function(result){
                    console.log("Adding weights succeed");
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("Adding weights failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        self.getPartsList = function (option, key) {
            var params = {};
            if(key){
                params = {searchOption: option, key: key};
            }
            var defer = $q.defer();
            $http
                .get("/api/parts", {params: params})
                .then(function(result){
                    console.log("search parts successful");
                    self.partsData = result.data;
                    defer.resolve(result);
                })
                .catch(function(err){
                    console.log("search parts failed");
                    defer.reject(err);
                });
            return defer.promise;
        };
        self.updatePart = function (parts) {
            var defer = $q.defer();
            $http
                .put("/api/parts", parts)
                .then(function(result){
                    console.log("update part successful");
                    defer.resolve(result);
                })
                .catch(function(err){
                    console.log("update part failed");
                    defer.reject(err);
                });
            return defer.promise;
        };

        self.addParts = function (parts) {
            console.log("--inventory.service--addParts--", parts);
            var deferred = $q.defer();
            $http
                .post("/api/parts", parts)
                .then(function(result){
                    console.log("Adding parts succeed");
                    deferred.resolve(result);
                })
                .catch(function(err){
                    console.log("Adding parts failed");
                    deferred.reject(err);
                });
            return deferred.promise;
        };
    }
})();
