(function(){
    angular
        .module("InventoryApp")
        .controller("CellsManagerCtrl", CellsManagerCtrl);

    CellsManagerCtrl.$inject = ["$state", "AuthFactory", "InventoryService"];
    function CellsManagerCtrl($state, AuthFactory, InventoryService){
        var vm = this;
        vm.cellcounts = 20;
        vm.columns = 10;
        vm.data = [];               // store data for all cells from database
        vm.node = [];               // store 2D data for page view
        vm.inventory_monitors = []; // weight data for all cess
        vm.itemsPerPage = 20;
        vm.currentPage = 1;
        vm.totalPages = 0;
        vm.isUserLogon = false;
        vm.editEn = false;
        vm.confirmUpdate = false;
        vm.updateCellEn = false;
        vm.cell = {};               // store single cell data for update purpose

        vm.cm_addCellInit = function(){
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if(!vm.isUserLogon){
                $state.go("login");
            }
        };

        vm.cm_init = function(){
            vm.isUserLogon = AuthFactory.isLoggedIn();
            if (vm.isUserLogon){
                InventoryService.getCells(null, null)
                    .then(function(success){
                        vm.data = InventoryService.cellData;
                        vm.cellcounts = vm.data.length;
                        InventoryService.getInventoryWeight({limit: vm.cellcounts})
                            .then(function(success1){
                                console.log("--cellsmanager.controller--init:sucess");
                                vm.inventory_monitors = InventoryService.inventory_monitors;
                                addWeightToData();
                                cellToNode();
                            }, function(err){
                                console.log("--cellsmanager.controller--init:err", err);
                            });
                    });
            }else{
                $state.go("login");
            }
        };

        function cellToNode(){
            var tempData = vm.data.slice(0);
            vm.node = [];
            while (tempData.length){
                if (tempData.length >= vm.columns){
                    vm.node.push(tempData.splice(0, vm.columns));
                }else{
                    vm.node.push(tempData.splice(0, tempData.length));
                }
            }
            console.log("vm.node", vm.node);
        }

        function addWeightToData(){
            console.log("vm_data", vm.data);
            var length = vm.data.length;
            for(var i = 0; i < length; i ++){
                var id = Number(vm.data[i].id);
                var weight = vm.inventory_monitors[id-1].weight;
                vm.data[i].weight = weight.toFixed(2);
            }
        }

        function prepareDataToUpdate(){
            console.log("-- prepareDataToUpdate--vm.data", vm.data);
            var data = vm.data;
            vm.data = [];
            var length = data.length;
            var index = 0;
            var row = 0;
            for(var i = 0; i < length; i ++){
                if (i < vm.cellcounts / 2){
                    index = i;
                    row = 0;
                }else{
                    index = i - vm.cellcounts / 2;
                    row = 1;
                }
                console.log("vm.node[row][index]", vm.node[row][index]);
                if (data[i].node_address !== vm.node[row][index].node_address ||
                    data[i].cell_address !== vm.node[row][index].cell_address ||
                    data[i].part_id !== vm.node[row][index].part_id ||
                    data[i].full_weight !== vm.node[row][index].full_weight ||
                    data[i].min_weight !== vm.node[row][index].min_weight ||
                    data[i].alarm_weight !== vm.node[row][index].alarm_weight
                ){
                    vm.data.push(vm.node[row][index]);
                }
            }
        }

        vm.cm_reload = function () {
            vm.editEn = false;
            vm.confirmUpdate = false;
            vm.cm_init();
        };
        vm.cm_submit = function () {
            vm.editEn = false;
            vm.confirmUpdate = false;
//            prepareDataToUpdate();
            console.log("--cellsmanager.controller--submit--vm.data", vm.data);
            var length = vm.data.length;
            var i = 1;
            var promise = true;
            // while(i < length){
                if(promise){
                    promise = false;
                    InventoryService.bulkUpdateCells(vm.data)
                        .then(function(success){
                            console.log("cell id=" + vm.data[i].id + " updated", success);
                            promise = true;
                            i = i + 1;
                        }, function(err){
                            console.log("cell id=" + vm.data[i].id + " updated with error:", err);
                        })
                        .catch(function(err){
                            console.log("cell id=" + vm.data[i].id + " failed due to error:", err);
                        });
                }
            // }

        };
        vm.cm_confirmUpdateCell = function(cell){
            vm.updateCellEn = true;
            vm.cell = cell;
        };

        vm.cm_updateCell = function(){
            console.log("--cellsmanager.controller--cm_updateCell--", vm.cell);
            vm.updateCellEn = false;
            InventoryService.updateCell(vm.cell)
                .then(function(success){
                    console.log("cell id=" + vm.cell.id + " updated", success);
                }, function(err){
                    console.log("cell id=" + vm.cell.id + " updated with error:", err);
                })
                .catch(function(err){
                    console.log("cell id=" + vm.cell.id + " failed due to error:", err);
                });
        };

        vm.cm_bulkUpdate = function () {
            // just set flag to enable confirm message and button
            // actual update operation is done by submit function
            vm.confirmUpdate = true;
            console.log("--cellsmanager.controller--update--vm.node", vm.node);
        };
        vm.cm_cancel = function () {
            vm.editEn = false;
            vm.confirmUpdate = false;
            vm.updateCellEn = false;
        };
        vm.cm_confirmNo = function () {
            vm.confirmUpdate = false
            vm.updateCellEn = false;
        };
        vm.cm_edit = function () {
            vm.editEn = true;
            vm.confirmUpdate = false;
        };

        vm.cm_add = function () {
            $state.go("addcell");
        };

        vm.cm_addWeight = function () {
            vm.editEn = false;
            vm.confirmUpdate = false;
            var weights = [];
            for (var i = 0; i < vm.cellcounts; i++){
                var weight = {};
                weight.cell_id = Number(vm.data[i].id);
                weight.quantity = 0;
                weight.weight = vm.data[i].weight;
                weights.push(weight);
            }
            InventoryService.addWeights(weights)
                .then(function(success){
                    console.log("Weights added", success);
                }, function(err){
                    console.log("Weights added with error:", err);
                })
                .catch(function(err){
                    console.log("Weights add failed due to error:", err);
                });
        };

        vm.cm_addCell = function(){
            vm.editEn = false;
            vm.confirmUpdate = false;
            InventoryService.addCells(vm.cell)
                .then(function(success){
                    console.log("Cells added", success);
                }, function(err){
                    console.log("Cells add failed due to error:", err);
                })
                .catch(function(err){
                    console.log("Cells add failed due to error:", err);
                });
            $state.go("cellsmanager");
        };

        vm.cm_cancelAddCell = function(){
            vm.editEn = false;
            vm.confirmUpdate = false;
            $state.go("cellsmanager");
        }
    }
})();