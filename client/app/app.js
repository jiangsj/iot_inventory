(function(){
    angular
        .module("InventoryApp",[
            "ui.router",
            "ngFlash",
            "ngSanitize",
            "highcharts-ng"
        ] );
})();