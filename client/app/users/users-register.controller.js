(function () {
    angular
        .module("InventoryApp")
        .controller("RegisterCtrl", ["$sanitize", "$state", "AuthFactory", "Flash", RegisterCtrl]);

    function RegisterCtrl($sanitize, $state, AuthFactory, Flash){
        var vm = this;

        vm.user = {
            employee_id: "",
            first_name: "",
            last_name: "",
            email: "",
            password: "",
            confirm_password: "",
            phone: "",
            access_group: ""
        };

        vm.access_options = ["User", "Purchaser", "Admin"];
        vm.user.access_group = vm.access_options[0];

        vm.register = function () {
            if(!vm.user.employee_id || !vm.user.first_name || !vm.user.last_name || !vm.user.email
                || !vm.user.password || (vm.user.password != vm.user.confirm_password) || !vm.user.access_group){
                Flash.clear();
                Flash.create('danger', 'Registration form has invalid fields, please correct and resubmit!');
                return;
            }
            var email = $sanitize(vm.user.email);
            var password = $sanitize(vm.user.password);
            vm.user.email = email;
            vm.user.password = password;
            console.log(vm.user);
            AuthFactory.register(vm.user)
                .then(function () {
                    vm.disabled = false;

                    vm.user = {};
                    Flash.clear();
                    Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {class: 'custom-class', id: 'custom-id'}, true);
                    $state.go("login");
                }).catch(function () {
                console.error("registration having issues");
            });
        };

    }
})();