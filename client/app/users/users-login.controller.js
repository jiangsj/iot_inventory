(function () {
    angular
        .module("InventoryApp")
        .controller("LoginCtrl", ["$state", "AuthFactory", "Flash", LoginCtrl]);

    function LoginCtrl($state, AuthFactory, Flash){
        var vm = this;

        vm.login = function () {
            console.log("--login()--", vm.user);
            AuthFactory.login(vm.user)
                .then(function () {
                    if(AuthFactory.isLoggedIn()){
                        console.log("logging in okay.. will go home (dashboard) now");
                        vm.emailAddress = "";
                        vm.password = "";
                        $state.go("home");
                    }else{
                        Flash.create('danger', "Ooops having issue logging in!", 0, {class: 'custom-class', id: 'custom-id'}, true);
                        $state.go("SignIn");
                    }
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }
})();