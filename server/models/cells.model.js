var Sequelize = require("sequelize");
var Parts = require("./parts.model");
// var Purchasers = require("./purchasers.model");

module.exports = function (database) {

    var Cells = database.define("cells", {

        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        node_address: {
            type: Sequelize.STRING,
            allowNull: false
        },
        cell_address: {
            type: Sequelize.STRING,
            allowNull: false
        },
        part_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "parts",
            referenceKey: "id",
            onUpdate: "cascade",
            allowNull: false
        },
        full_weight: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        min_weight: {
            type: Sequelize.FLOAT,
            allowNull: false
        },
        alarm_weight: {
            type: Sequelize.FLOAT,
            allowNull: false
        }
    },{
        freezeTableName: true,
        timestamps:true
    });

    return Cells;

};