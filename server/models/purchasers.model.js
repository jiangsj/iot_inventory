var Sequelize = require("sequelize");
var User = require("./users.model.js");
var Vendor = require("./vendors.model.js");

// employeeId – String
// vendorName - String

module.exports = function (database) {

    var Purchaser = database.define("purchasers", {
        in: {
            type: Sequelize.INTEGER.UNSIGNED,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false
        },
        user_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "users",
            referenceKey: "id",
            onUpdate: "cascade",
            allowNull: false
        },
        vendor_id: {
            type: Sequelize.INTEGER.UNSIGNED,
            reference: "vendors",
            refreenceKey: "id",
            onUpdate: "cascade",
            allowNull: false
        }
     },{
        freezeTableName: true,
        timestamps:true
    });

    return Purchaser;

};

