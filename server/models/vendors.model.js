var Sequelize = require("sequelize");

module.exports = function (database) {

    var Vendors = database.define("vendors", {

        id: {
            type: Sequelize.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true
        },
        vendor_name: {
            type: Sequelize.STRING,
            allowNull: false
        },
        vendor_phone: {
            type: Sequelize.STRING,
            allowNull: true
        },
        vendor_email: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },{
        freezeTableName: true,
        timestamps:true
    });

    return Vendors;

};