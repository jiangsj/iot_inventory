var Parts = require("../../database").Part;

console.log("---Parts", Parts);
function searchCriteria(query){
    var where = {};
    if (query.key !== undefined){
        if (query.searchOption !== undefined){
            switch (query.searchOption){
                case "1":
                    // All
                    where = {
                        $or: [
                            {part_number: {$like: "%" + query.key + "%"}},
                            {part_name: {$like: "%" + query.key + "%"}},
                            {mfc_part_number: {$like: "%" + query.key + "%"}},
                            {vendor_id: {$like: "%" + query.key + "%"}},
                            {vendor_part_number: {$like: "%" + query.key + "%"}},
                            {purchaser_id: {$like: "%" + query.key + "%"}}
                        ]
                    };
                    break;
                case "2":
                    // By Part Number
                    where = {part_number: {$like: "%" + query.key + "%"}};
                    break;
                case "3":
                    // By Part Name
                    where = {part_name: {$like: "%" + query.key + "%"}};
                    break;
                case "4":
                    // By Manufacture Part Number
                    where = {mfc_part_number: {$like: "%" + query.key + "%"}};
                    break;
                case "5":
                    // By Part Number
                    where = {vendor_id: {$like: "%" + query.key + "%"}};
                    break;
                case "6":
                    // By Vendor Part Number
                    where = {vendor_part_number: {$like: "%" + query.key + "%"}};
                    break;
                case "7":
                    // By Vendor Part Number
                    where = {purchaser_id: {$like: "%" + query.key + "%"}};
                    break;
                default:
                    break;

            }
        }
    }
    console.log("where", where);
    return where;
}
exports.list = function(req,res){
    console.log("reached parts database >> list");
    var whereCondition = searchCriteria(req.query);
    Parts
        .findAll({
            where: whereCondition,
            limit: 20
        })
        .then(function (result) {
            if (result) {
                console.log(result);
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.delete = function(req,res){
    console.log("--reached parts database--delete");
    console.log(req.params);
    Parts
        .destroy({
            where: {
                id: req.params.partId
            }
        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res) {
    console.log("--reached parts database--update");
    console.log(req.body);

    // if (req.body.length === 1){
        // single part update
        Parts
            .find({
                where: {
                    id: Number(req.body.id)
                }
            })
            .then(function (response) {
                console.log('DB Response', response);
                response.updateAttributes({
                    part_number: req.body.part_number,
                    part_name: req.body.part_name,
                    mfc_part_number: req.body.mfc_part_number,
                    unit: req.body.unit,
                    unit_price: req.body.unit_price,
                    unit_weight: req.body.unit_weight,
                    vendor_id: req.body.vendor_id,
                    vendor_part_number: req.body.vendor_part_number,
                    purchaser_id: req.body.purchaser_id
                }).then(function () {
                    console.log("update done");
                    res.status(200).end();
                }).catch(function () {
                    console.log("update failed");
                    res
                        .status(500)
                        .json({error: true, errorText: "Update Failed"})
                });
            })
            .catch(function (err) {
                console.log("err", err);
                res
                    .status(500)
                    .json({error: true, errorText: "Record not found"})
            });
    // }else{
    //     // bulk parts update
    // }

};

exports.add = function(req,res){
    console.log("--reached parts database--add");
    console.log(req.body);
    if(req.body.length === undefined){
        Parts
            .create({
                part_number: req.body.part_number,
                part_name: req.body.part_name,
                mfc_part_number: req.body.mfc_part_number,
                unit: req.body.unit,
                unit_price: Number(req.body.unit_price),
                unit_weight: Number(req.body.unit_weight),
                vendor_id: Number(req.body.vendor_id),
                vendor_part_number: req.body.vendor_part_number,
                purchaser_id: Number(req.body.purchaser_id)
            })
            .then(function(part){
                console.log("New part added");
                res.status(201).json(part);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add part failed"})
            });
    }else{
        Parts
            .bulkCreate(
               req.body
            )
            .then(function(part){
                console.log("New part added");
                res.status(201).json(part);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add part failed"})
            });
    }
};
