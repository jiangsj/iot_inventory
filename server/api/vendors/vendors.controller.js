var Vendors = require("../../database").Vendor;


exports.list = function(req,res){
    console.log("reached vendors database >> list");
    Vendors
        .findAll({
            limit: 20
        })
        .then(function (result) {
            if (result) {
                console.log(result);
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.listByName = function(req,res){
    console.log("reached vendors database >> list by name");
    console.log(req.params);
    Vendors
        .findAll({
            where: {
                   vendor_name: {
                        $like: "%" + req.params.name + "%"
                    }
            },
            limit: 20
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.listByEmail = function(req,res){
    console.log("reached vendors database >> list by email");
    console.log(req.params);
    Vendors
        .findAll({
            where: {
                vendor_email: {
                    $like: "%" + req.params.email + "%"
                }
            },
            limit: 20
        })
        .then(function (result) {
            if (result) {
                res.json(result);
            } else {
                res.status(400).send(JSON.stringify("Record Not Found"));
            }
        });
};

exports.delete = function(req,res){
    console.log("--reached vendors database--delete");
    console.log(req.params);
    Vendors
        .destroy({
            where: {
                id: req.params.vendorId
            }
        })
        .then(function(result) {
            console.log("deleted");
            res
                .status(200)
                .json(result)
        })
        .catch(function(err){
            console.log("err", err);
            res
                .status(500)
                .json({error: true})
        })

};

exports.update = function(req,res) {
    console.log("--reached vendors database--update");
    console.log(req.params);
    Vendors
        .find({
            where: {
                id: Number(req.params.vendorId)
            }
        })
        .then(function (response) {
            console.log('DB Response', response);
            response.updateAttributes({
                vendor_name: req.body.vendor_name,
                vendor_phone: req.body.vendor_phone,
                vendor_email: req.body.vendor_email
            }).then(function () {
                console.log("update done");
                res.status(200).end();
            }).catch(function () {
                console.log("update failed");
                res
                    .status(500)
                    .json({error: true, errorText: "Update Failed"})
            });
        })
        .catch(function (err) {
            console.log("err", err);
            res
                .status(500)
                .json({error: true, errorText: "Record not found"})
        });
};

exports.add = function(req,res){
        console.log("--reached vendors database--add");
        console.log(req.body);
        Vendors
            .create({
                vendor_name: req.body.vendor_name,
                vendor_phone: req.body.vendor_phone,
                vendor_email: req.body.vendor_email
            })
            .then(function(vendor){
                console.log("New vender added");
                    res.status(201).json(vendor);
            })
            .catch(function(err){
                console.log("err", err);
                res
                    .status(409)
                    .json({error: true, errorText: "Add vendor failed"})
            });
};
